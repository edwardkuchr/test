﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using DevExpress.Diagram.Core;
using DevExpress.Diagram.Core.Native;
using DevExpress.Utils;
using DevExpress.XtraDiagram;
using DevExpress.XtraSpellChecker.Parser;
using System.Xml.Serialization;
using DevExpress.Diagram.Core.Themes;
using DevExpress.Utils.Animation;
using DevExpress.Utils.Extensions;
using DevExpress.XtraDiagram.Base;

namespace test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();





            /*        // индикатор Sensor2

                   DiagramShape rectangle_Sensor2 = new DiagramShape();
                   rectangle_Sensor2.Size = new Size(100, 55);
                       rectangle_Sensor2.Position = new PointFloat(100, -200);
                       rectangle_Sensor2.StrokeId = DiagramThemeColorId.Black;
                       rectangle_Sensor2.ForegroundId = DiagramThemeColorId.Black;
                       rectangle_Sensor2.Content = "22";
                    ((IDiagramItem) rectangle_Sensor2).Background= new SolidColorBrushInfo(Color.Red);

                         // индикатор Sensor3
                          DiagramShape rectangle_Sensor3 = new DiagramShape();
                          rectangle_Sensor3.Size = new Size(100, 55);
                          rectangle_Sensor3.Position = new PointFloat(100, 70);
                          rectangle_Sensor3.BackgroundId = DiagramThemeColorId.Accent1_4;
                          rectangle_Sensor3.StrokeId = DiagramThemeColorId.Black;
                          rectangle_Sensor3.ForegroundId = DiagramThemeColorId.Black;
                          rectangle_Sensor3.Content = "??";
                          */

            //  diagramControl1.Items.Add(rectangle_Sensor2);
            //  diagramControl1.Items.Add(rectangle_Sensor3);
        }

        private Diagram diagram; // диаграмма из xml
        private void button_load_Click(object sender, EventArgs e)
        {
            try
            {



                using (OpenFileDialog dialog = new OpenFileDialog())
                {
                    dialog.InitialDirectory = Assembly.GetExecutingAssembly().Location;

                    dialog.Filter = "XML файлы|*.xml";
                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        using (FileStream fs = new FileStream(dialog.FileName, FileMode.OpenOrCreate))
                        {
                            XmlSerializer formatter = new XmlSerializer(typeof(Diagram));
                            diagram = (Diagram) formatter.Deserialize(fs);
                        }

                    }

                    foreach (Xml item in diagram.Elements.HeatingPanel) // пробежим по списку щитков и отрисуем
                    {
                        CreateHeatingPanel(item.Id, item.Name, item.Location, item.Temperature, item.IsInAlarm,
                            item.IsEntryAutomateOn, item.IsNetworkOn, item.IsPowerOn, item.IsOnUps);

                    }

                    foreach (HeatingLine item in diagram.Elements.HeatingLine) // пробежим по списку линии обогрева и отрисуем
                    {
                        CreateHeatingLine(item.Id, item.Name, item.Location, item.Temperature, item.ParentId,
                            item.State);
                    }
                   foreach (Sensor item in diagram.Elements.Sensor) // пробежим по списку сенсоров и отрисуем
                    {
                        CreateSensor(item.Id, item.Name, item.Location, item.Temperature, item.ParentId,
                            item.State);
                    }
                }
            }

            catch (Exception exception)
            {
                MessageBox.Show("Ошибка при десериализации файла: " + exception.Message);
                //throw;
            }

        }
        // метод отрисовки щита
        private void CreateHeatingPanel(string Id, string Name, string Location, string Temp, string IsInAlarm, string IsEntryAutomateOn, string IsNetworkOn, string IsPowerOn, string IsOnUps)
        {
            try
            {
                float x = Convert.ToSingle(Location.Substring(0, Location.IndexOf(';'))); // координаты где выводить щиток
               float y = Convert.ToSingle(Location.Substring(Location.IndexOf(';')+1));
                DiagramContainer diagramContainer = new DiagramContainer(x,y, 100, 150);
                ((IDiagramItem) diagramContainer).FontSize=12;
                ((IDiagramItem)diagramContainer).FontWeight = FontWeightKind.Bold;
                diagramContainer.Shape = StandardContainers.Alternating; //стиль контейнера
                diagramContainer.ShowHeader = true;
                diagramContainer.Header = Name;

                diagramContainer.BackgroundId = DiagramThemeColorId.Accent1;
                diagramContainer.ForegroundId = DiagramThemeColorId.Black;
                diagramContainer.StrokeId = DiagramThemeColorId.Black;


                //////круглые индикаторы
                DiagramShape circle_IsEntryAutomateOn = new DiagramShape(BasicShapes.Ellipse);
                circle_IsEntryAutomateOn.Size = new Size(15, 15);
                circle_IsEntryAutomateOn.Position = new PointFloat(81, 5);

                if (Convert.ToSingle(IsEntryAutomateOn) == 1)
                {
                    ((IDiagramItem)circle_IsEntryAutomateOn).Background = new SolidColorBrushInfo(Color.Green);
                }
                if (Convert.ToSingle(IsEntryAutomateOn) == 0)
                {
                    ((IDiagramItem)circle_IsEntryAutomateOn).Background = new SolidColorBrushInfo(Color.Red);
                }
                if (Convert.ToSingle(IsEntryAutomateOn) <0)
                {
                    ((IDiagramItem)circle_IsEntryAutomateOn).Background = new SolidColorBrushInfo(Color.Gray);
                }
                
                circle_IsEntryAutomateOn.StrokeId = DiagramThemeColorId.White;
                diagramContainer.Items.Add(circle_IsEntryAutomateOn);

                DiagramShape circle_IsNetworkOn = new DiagramShape(BasicShapes.Ellipse);
                circle_IsNetworkOn.Size = new Size(15, 15);
                circle_IsNetworkOn.Position = new PointFloat(81, 20);
                if (Convert.ToSingle(IsNetworkOn) == 1)
                {
                    ((IDiagramItem)circle_IsNetworkOn).Background = new SolidColorBrushInfo(Color.Green);
                }
                if (Convert.ToSingle(IsNetworkOn) == 0)
                {
                    ((IDiagramItem)circle_IsNetworkOn).Background = new SolidColorBrushInfo(Color.Red);
                }
                if (Convert.ToSingle(IsNetworkOn)  <0)
                {
                    ((IDiagramItem)circle_IsNetworkOn).Background = new SolidColorBrushInfo(Color.Gray);
                }
                circle_IsNetworkOn.StrokeId = DiagramThemeColorId.White;
                diagramContainer.Items.Add(circle_IsNetworkOn);

                DiagramShape circle_IsPowerOn = new DiagramShape(BasicShapes.Ellipse);
                circle_IsPowerOn.Size = new Size(15, 15);
                circle_IsPowerOn.Position = new PointFloat(81, 35);
                if (Convert.ToSingle(IsPowerOn) == 1)
                {
                    ((IDiagramItem)circle_IsPowerOn).Background = new SolidColorBrushInfo(Color.Green);
                }
                if (Convert.ToSingle(IsPowerOn) == 0)
                {
                    ((IDiagramItem)circle_IsPowerOn).Background = new SolidColorBrushInfo(Color.Red);
                }
                if (Convert.ToSingle(IsPowerOn) < 0)
                {
                    ((IDiagramItem)circle_IsPowerOn).Background = new SolidColorBrushInfo(Color.Gray);
                }
                circle_IsPowerOn.StrokeId = DiagramThemeColorId.White;
                diagramContainer.Items.Add(circle_IsPowerOn);


                DiagramShape circle_IsOnUps = new DiagramShape(BasicShapes.Ellipse);
                circle_IsOnUps.Size = new Size(15, 15);
                circle_IsOnUps.Position = new PointFloat(81, 50);
                if (Convert.ToSingle(IsOnUps) == 1)
                {
                    ((IDiagramItem)circle_IsOnUps).Background = new SolidColorBrushInfo(Color.Green);
                }
                if (Convert.ToSingle(IsOnUps) == 0)
                {
                    ((IDiagramItem)circle_IsOnUps).Background = new SolidColorBrushInfo(Color.Red);
                }
                if (Convert.ToSingle(IsOnUps) < 0)
                {
                    ((IDiagramItem)circle_IsOnUps).Background = new SolidColorBrushInfo(Color.Gray);
                }
                circle_IsOnUps.StrokeId = DiagramThemeColorId.White;
                diagramContainer.Items.Add(circle_IsOnUps);
                ////

                
                //// подписи к индикаторам
                DiagramShape rectangle_Signature1 = new DiagramShape();
                rectangle_Signature1.Size = new Size(60, 15);
                rectangle_Signature1.Position = new PointFloat(8, 5);
                rectangle_Signature1.BackgroundId = DiagramThemeColorId.Accent1;
                rectangle_Signature1.StrokeId = DiagramThemeColorId.Accent1;
                rectangle_Signature1.ForegroundId = DiagramThemeColorId.Black;
                rectangle_Signature1.Content = "Вв.автомат";
                ((IDiagramItem)rectangle_Signature1).TextAlignment = TextAlignmentKind.Left;
                diagramContainer.Items.Add(rectangle_Signature1);

                DiagramShape rectangle_Signature2 = new DiagramShape();
                rectangle_Signature2.Size = new Size(60, 15);
                rectangle_Signature2.Position = new PointFloat(8, 20);
                rectangle_Signature2.BackgroundId = DiagramThemeColorId.Accent1;
                rectangle_Signature2.StrokeId = DiagramThemeColorId.Accent1;
                rectangle_Signature2.ForegroundId = DiagramThemeColorId.Black;
                rectangle_Signature2.Content = "Связь";
                ((IDiagramItem)rectangle_Signature2).TextAlignment = TextAlignmentKind.Left;
                diagramContainer.Items.Add(rectangle_Signature2);

                DiagramShape rectangle_Signature3 = new DiagramShape();
                rectangle_Signature3.Size = new Size(60, 15);
                rectangle_Signature3.Position = new PointFloat(8, 35);
                rectangle_Signature3.BackgroundId = DiagramThemeColorId.Accent1;
                rectangle_Signature3.StrokeId = DiagramThemeColorId.Accent1;
                rectangle_Signature3.ForegroundId = DiagramThemeColorId.Black;
                rectangle_Signature3.Content = "Питание";
                ((IDiagramItem)rectangle_Signature3).TextAlignment = TextAlignmentKind.Left;
                diagramContainer.Items.Add(rectangle_Signature3);

                DiagramShape rectangle_Signature4 = new DiagramShape();
                rectangle_Signature4.Size = new Size(60, 15);
                rectangle_Signature4.Position = new PointFloat(8, 50);
                rectangle_Signature4.BackgroundId = DiagramThemeColorId.Accent1;
                rectangle_Signature4.StrokeId = DiagramThemeColorId.Accent1;
                rectangle_Signature4.ForegroundId = DiagramThemeColorId.Black;
                rectangle_Signature4.Content = "На ИБП";
                ((IDiagramItem)rectangle_Signature4).TextAlignment = TextAlignmentKind.Left;
                diagramContainer.Items.Add(rectangle_Signature4);

                DiagramShape rectangle_SignatureTemp = new DiagramShape();
                rectangle_SignatureTemp.Size = new Size(70, 15);
                rectangle_SignatureTemp.Position = new PointFloat(8, 65);
                rectangle_SignatureTemp.BackgroundId = DiagramThemeColorId.Accent1;
                rectangle_SignatureTemp.StrokeId = DiagramThemeColorId.Accent1;
                rectangle_SignatureTemp.ForegroundId = DiagramThemeColorId.Black;
                rectangle_SignatureTemp.Content = "Темп. "+Temp+ "°" + "C";
                ((IDiagramItem)rectangle_SignatureTemp).TextAlignment = TextAlignmentKind.Left;
                diagramContainer.Items.Add(rectangle_SignatureTemp);
                /// 

                // индикатор авария
                if (IsInAlarm == "true")
                {
                    DiagramShape rectangle_IsInAlarm = new DiagramShape();
                    rectangle_IsInAlarm.Size = new Size(80, 40);
                    rectangle_IsInAlarm.Position = new PointFloat(12, 83);
                    ((IDiagramItem)rectangle_IsInAlarm).Background = new SolidColorBrushInfo(Color.Red);
                    rectangle_IsInAlarm.StrokeId = DiagramThemeColorId.Black;
                    rectangle_IsInAlarm.ForegroundId = DiagramThemeColorId.Black;
                    rectangle_IsInAlarm.Content = "Авария";
                    diagramContainer.Items.Add(rectangle_IsInAlarm);
                }

                //вывод контейнера
                diagramControl1.Items.Add(diagramContainer);
            }
            catch (Exception e)
            {
                MessageBox.Show("Ошибка в методе вывода щитка: "+e.Message);
              //  throw;
            }
        }
        // метод отрисовки линии подогрева
        private void CreateHeatingLine(string Id, string Name, string Location, string Temp, string ParentId, string State)
        {
            try
            {
                // индикатор HeatingLine
                DiagramShape rectangle_HeatingLine = new DiagramShape();

                rectangle_HeatingLine.Size = new Size(80, 40);
                float x = Convert.ToSingle(Location.Substring(0, Location.IndexOf(';'))); // координаты где выводить линию обогрева
                float y = Convert.ToSingle(Location.Substring(Location.IndexOf(';') + 1));
                rectangle_HeatingLine.Position = new PointFloat(x, y);
               // rectangle_HeatingLine.BackgroundId = DiagramThemeColorId.Accent6_4;
                if (State=="Alarm")
                {
                    ((IDiagramItem)rectangle_HeatingLine).Background = new SolidColorBrushInfo(Color.Red);
                }
                if (State == "Warning")
                {
                    ((IDiagramItem)rectangle_HeatingLine).Background = new SolidColorBrushInfo(Color.Yellow);
                }
                if (State== "GoodOrOff")
                {
                    ((IDiagramItem)rectangle_HeatingLine).Background = new SolidColorBrushInfo(Color.Gray);
                }
                rectangle_HeatingLine.StrokeId = DiagramThemeColorId.Black;
                rectangle_HeatingLine.ForegroundId = DiagramThemeColorId.Black;
                rectangle_HeatingLine.Content = Temp;
                diagramControl1.Items.Add(rectangle_HeatingLine);
            }
            catch (Exception e)
            {
                MessageBox.Show("Ошибка в методе вывода линии обогрева: "+e.Message);
                throw;
            }
        }
        // метод отрисовки сенсоров
        private void CreateSensor(string Id, string Name, string Location, string Temp, string ParentId, string State)
        {
            try
            {
                // индикатор Sensor
                DiagramShape rectangle_Sensor = new DiagramShape();
                rectangle_Sensor.Shape= DevExpress.Diagram.Core.BasicShapes.RoundedRectangle;
                rectangle_Sensor.Size = new Size(80, 40);
                float x = Convert.ToSingle(Location.Substring(0, Location.IndexOf(';'))); // координаты где выводить сенсоры
                float y = Convert.ToSingle(Location.Substring(Location.IndexOf(';') + 1));
                rectangle_Sensor.Position = new PointFloat(x, y);
               
                if (State == "Alarm")
                {
                    ((IDiagramItem)rectangle_Sensor).Background = new SolidColorBrushInfo(Color.Red);
                }
                if (State == "Warning")
                {
                    ((IDiagramItem)rectangle_Sensor).Background = new SolidColorBrushInfo(Color.Yellow);
                }
                if (State == "GoodOrOff")
                {
                    ((IDiagramItem)rectangle_Sensor).Background = new SolidColorBrushInfo(Color.Green);
                }
                rectangle_Sensor.StrokeId = DiagramThemeColorId.Black;
                rectangle_Sensor.ForegroundId = DiagramThemeColorId.Black;
                rectangle_Sensor.Content = Temp;
                diagramControl1.Items.Add(rectangle_Sensor);
            }
            catch (Exception e)
            {
                MessageBox.Show("Ошибка в методе вывода сенсоров: " + e.Message);
                //throw;
            }
        }
    }
}
