﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace test
{
 
    [XmlRoot(ElementName = "HeatingPanel")]
    public class Xml
    {
        [XmlAttribute(AttributeName = "Id")]
        public string Id { get; set; }
        [XmlAttribute(AttributeName = "Name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "Location")]
        public string Location { get; set; }
        [XmlAttribute(AttributeName = "Temperature")]
        public string Temperature { get; set; }
        [XmlAttribute(AttributeName = "IsInAlarm")]
        public string IsInAlarm { get; set; }
        [XmlAttribute(AttributeName = "IsEntryAutomateOn")]
        public string IsEntryAutomateOn { get; set; }
        [XmlAttribute(AttributeName = "IsNetworkOn")]
        public string IsNetworkOn { get; set; }
        [XmlAttribute(AttributeName = "IsPowerOn")]
        public string IsPowerOn { get; set; }
        [XmlAttribute(AttributeName = "IsOnUps")]
        public string IsOnUps { get; set; }
    }

    [XmlRoot(ElementName = "HeatingLine")]
    public class HeatingLine
    {
        [XmlAttribute(AttributeName = "Id")]
        public string Id { get; set; }
        [XmlAttribute(AttributeName = "Name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "Location")]
        public string Location { get; set; }
        [XmlAttribute(AttributeName = "Temperature")]
        public string Temperature { get; set; }
        [XmlAttribute(AttributeName = "ParentId")]
        public string ParentId { get; set; }
        [XmlAttribute(AttributeName = "State")]
        public string State { get; set; }
    }

    [XmlRoot(ElementName = "Sensor")]
    public class Sensor
    {
        [XmlAttribute(AttributeName = "Id")]
        public string Id { get; set; }
        [XmlAttribute(AttributeName = "Name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "Location")]
        public string Location { get; set; }
        [XmlAttribute(AttributeName = "Temperature")]
        public string Temperature { get; set; }
        [XmlAttribute(AttributeName = "ParentId")]
        public string ParentId { get; set; }
        [XmlAttribute(AttributeName = "State")]
        public string State { get; set; }
    }

    [XmlRoot(ElementName = "Elements")]
    public class Elements
    {
        [XmlElement(ElementName = "HeatingPanel")]
        public List<Xml> HeatingPanel { get; set; }
        [XmlElement(ElementName = "HeatingLine")]
        public List<HeatingLine> HeatingLine { get; set; }
        [XmlElement(ElementName = "Sensor")]
        public List<Sensor> Sensor { get; set; }
    }

    [XmlRoot(ElementName = "Diagram")]
    public class Diagram
    {
        [XmlElement(ElementName = "Elements")]
        public Elements Elements { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
        [XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsd { get; set; }
    }
}
